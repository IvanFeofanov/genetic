#include "schemeditor.h"
#include "ui_schemeditor.h"

#include "basealgorithms.h"

#include <QGraphicsScene>
#include <QGraphicsSceneMouseEvent>
#include <QInputDialog>
#include <QPolygonF>
#include <QTimer>
#include <QTreeWidget>

#include <set>

#include <iostream>
#include "assert.h"

// общие роли графических элементов сцены
static const int c_nTypeRole = 0;

// роли элементов связей
static const int c_nLinkLayerRole = 1;

// типы объектов
enum ETypes
{
    eT_Schem,
    eT_Link
};

// Кисть для линий связей
static const QPen c_linkPen(GenerateAndGetColorByIndex(1, 0, 127),
                            1.5, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin);

static const QSizeF c_pageSize(300, 210);	// Размер листа, mm
static const float c_fGrid = 2.54;			// Шаг сетки, mm
static const float c_fPpmm = 100;			// Пикселей на mm

static const float c_fPadR = 0.75;			// радиус пояска отверстия
static const float c_fBoundPadR = c_fPadR + c_fPadR / 10; //

/// Перевести mm в пиксели
float mmToPx(float fMm_) { return fMm_ * c_fPpmm; }
/// Перевести пиксели в mm
float pxToMm(float fPx_) { return fPx_ / c_fPpmm; }

// Кисть для линий контуров элементов
static const QPen c_elementPen(Qt::gray, mmToPx(0.25));


//========================================================================================
SchemEditor::SchemEditor(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SchemEditor),
    m_pToolActGroup(new QActionGroup(this))
{
    ui->setupUi(this);

    // сцена
    CSchemGraphicsScene* pScene = new CSchemGraphicsScene(this);
    pScene->SetPage(c_pageSize);
    ui->gvSchem->setScene(pScene);
    ui->gvSchem->setRenderHint(QPainter::Antialiasing);

    // Экшены
    m_pToolActGroup->addAction(ui->actEditLink);
    m_pToolActGroup->addAction(ui->actHand);
    m_pToolActGroup->addAction(ui->actMove);
    m_pToolActGroup->addAction(ui->actDelete);
    m_pToolActGroup->addAction(ui->actAddElement);

    connect(ui->actAddElement, SIGNAL(toggled(bool)), this, SLOT(actAddElement_triggered(bool)));
    connect(ui->actZoomAll, SIGNAL(triggered()), this, SLOT(ZoomAll()));

    connect(ui->actEditLink, SIGNAL(toggled(bool)), this, SLOT(actEditLink_triggered(bool)));
    connect(ui->actHand, SIGNAL(toggled(bool)), this, SLOT(actHand_triggered(bool)));
    connect(ui->actMove, SIGNAL(toggled(bool)), this, SLOT(actMove_triggered(bool)));
    connect(ui->actDelete, SIGNAL(toggled(bool)), this, SLOT(actDelete_toggled(bool)));
    connect(m_pToolActGroup, SIGNAL(triggered(QAction*)), this, SLOT(toolActGroup_triggered(QAction*)));

    connect(ui->trwLayers, SIGNAL(itemChanged(QTreeWidgetItem*,int)), this, SLOT(trwLayers_itemChanged(QTreeWidgetItem*,int)));

    //Параметры
    connect(ui->swSettings, SIGNAL(currentChanged(int)), this, SLOT(swSettings_currentChanged(int)));
    connect(ui->dsbRouteWidth, SIGNAL(valueChanged(double)), pScene, SLOT(SetRouteWidth(double)));
    connect(ui->dsbSheetWidth, SIGNAL(valueChanged(double)), this, SLOT(dsbSheetWidth_valueChanged(double)));
    connect(ui->dsbSheetHeight, SIGNAL(valueChanged(double)), this, SLOT(dsbSheetHeight_valueChanged(double)));
    connect(ui->sbNumberOutputs, SIGNAL(valueChanged(int)), this, SLOT(sbNumberOutputs_valueChanged(int)));

    // По умолчанию включено перемещение
    ui->actMove->setChecked(true);

    // Показать всю сцену
    QTimer::singleShot(100, Qt::PreciseTimer, this, SLOT(ZoomAll()));
}

SchemEditor::~SchemEditor()
{
    delete ui;
}

void SchemEditor::setEditable(bool bEnabled_)
{
    ui->actAddElement->setEnabled(bEnabled_);
    ui->actDelete->setEnabled(bEnabled_);
    ui->actEditLink->setEnabled(bEnabled_);
    ui->actMove->setEnabled(bEnabled_);

    if(!bEnabled_)
        ui->swSettings->setCurrentIndex(3);
}

void SchemEditor::ExportSchem(SSchem &schem_) const
{
    static_cast<CSchemGraphicsScene*>(ui->gvSchem->scene())->ExportSchem(schem_);
}

void SchemEditor::ImportSchem(const SSchem &schem_)
{
    static_cast<CSchemGraphicsScene*>(ui->gvSchem->scene())->ImportSchem(schem_);
    ZoomAll();
    refresh_trwLayers(schem_);
}

void SchemEditor::Clear()
{
    CSchemGraphicsScene* pScene = static_cast<CSchemGraphicsScene*>(ui->gvSchem->scene());
    assert(pScene);
    pScene->Clear();
    pScene->SetPage(c_pageSize);
    ZoomAll();
}

//void SchemEditor::DrawRect(const QRectF &rect_)
//{
//    QGraphicsRectItem* rectItem = new QGraphicsRectItem(rect_);
//    QPen pen;
//    pen.setWidth(1);
//    pen.setCosmetic(true);
//    rectItem->setPen(pen);
//    ui->gvSchem->scene()->addItem(rectItem);
//}

void SchemEditor::ZoomAll()
{
    CSchemGraphicsScene* pScene = static_cast<CSchemGraphicsScene*>(ui->gvSchem->scene());
    assert(pScene);
    ui->gvSchem->fitInView(pScene->GetPageRect(), Qt::KeepAspectRatio);
}

void SchemEditor::toolActGroup_triggered(QAction *pAction_)
{
    // Если не один из инструментов не выбран
    bool bAnythingChecked = false;
    QList<QAction*> listAction = m_pToolActGroup->actions();
    for(QAction* pAction : listAction)
        bAnythingChecked |= pAction->isChecked();

    // Ставим виджет параметров листа
    if(!bAnythingChecked)
        ui->swSettings->setCurrentIndex(0);
}

void SchemEditor::swSettings_currentChanged(int nWidget)
{
    CSchemGraphicsScene* pScene = static_cast<CSchemGraphicsScene*>(ui->gvSchem->scene());
    if(!pScene)
        return;

    switch(nWidget)
    {
    case eSt_Sheet:
        ui->dsbSheetWidth->setValue(pScene->GetPageSize().width());
        ui->dsbSheetHeight->setValue(pScene->GetPageSize().height());
        break;

    case eSt_Element:
        // TODO
        break;

    case eSt_Route:
        ui->dsbRouteWidth->setValue(pScene->GetRouteWidth());
        break;

    default:
        break;
    }
}

void SchemEditor::dsbSheetWidth_valueChanged(double dW_)
{
    if(CSchemGraphicsScene* pScene = static_cast<CSchemGraphicsScene*>(ui->gvSchem->scene()))
    {
        QSizeF sheetSize = pScene->GetPageSize();
        sheetSize.setWidth(dW_);
        pScene->SetPage(sheetSize);
    }
}

void SchemEditor::dsbSheetHeight_valueChanged(double dH_)
{
    if(CSchemGraphicsScene* pScene = static_cast<CSchemGraphicsScene*>(ui->gvSchem->scene()))
    {
        QSizeF sheetSize = pScene->GetPageSize();
        sheetSize.setHeight(dH_);
        pScene->SetPage(sheetSize);
    }
}

void SchemEditor::sbNumberOutputs_valueChanged(int nValue_)
{
    //TODO
}

void SchemEditor::trwLayers_itemChanged(QTreeWidgetItem *pItem_, int nCol_)
{
    if(nCol_ == 0 && pItem_)
        filterLayers();
}

void SchemEditor::refresh_trwLayers(const SSchem& schem_)
{
    ui->trwLayers->clear();
    ui->trwLayers->blockSignals(true);

    QTreeWidgetItem* pRootItem = ui->trwLayers->invisibleRootItem();
    int nLayers = SSchem::GetSchemLayers(schem_);

    std::vector<QColor> palette;
    GeneratePalette(palette, nLayers);
    for(int i = 0; i < nLayers; i++)
    {
        QTreeWidgetItem* pItem = new QTreeWidgetItem(pRootItem);
        pItem->setText(0, QString("Слой %1").arg(i + 1));
        // иконка
        QPixmap pixmap(QSize(22, 22));
        pixmap.fill(palette[i]);
        pItem->setIcon(0, QIcon(pixmap));
        // флаг
        pItem->setFlags((pItem->flags() & ~Qt::ItemIsSelectable) | Qt::ItemIsUserCheckable);
        pItem->setCheckState(0, Qt::Checked);
    }

    // Обновить фильтр слоев
    filterLayers();

    ui->trwLayers->blockSignals(false);
}

void SchemEditor::filterLayers()
{
    // получить множество включеных слоев
    std::set<int> setEnabledLayers;
    QTreeWidgetItem* pRootItem = ui->trwLayers->invisibleRootItem();
    int nLayers = pRootItem->childCount();
    for(int i = 0; i < nLayers; i++)
    {
        QTreeWidgetItem* pItem = pRootItem->child(i);
        if(pItem && pItem->checkState(0) == Qt::Checked)
            setEnabledLayers.insert(i);
    }

    // прячем не включенные слои
    QList<QGraphicsItem*> listItems = ui->gvSchem->items();
    for(QGraphicsItem* pItem : listItems)
    {
        // только связи
        bool bOk;
        int nType = pItem->data(c_nTypeRole).toInt(&bOk);
        if(!bOk) continue;
        if(nType == eT_Link)
        {
            QGraphicsItemGroup* pLinkItem = static_cast<QGraphicsItemGroup*>(pItem);
            for(QGraphicsItem* pLineItem : pLinkItem->childItems())
            {
                QGraphicsLineItem* pSubLineItem = static_cast<QGraphicsLineItem*>(pLineItem);
                int nLayer = pSubLineItem->data(c_nLinkLayerRole).toInt(&bOk);
                assert(bOk);

                std::set<int>::const_iterator itFind = setEnabledLayers.find(nLayer);
                pItem->setVisible(itFind != setEnabledLayers.cend());
            }
        }
    }
}

void SchemEditor::disableAllTools()
{
    if(QAction* pAction = m_pToolActGroup->checkedAction())
        pAction->setChecked(false);
}

void SchemEditor::actAddElement_triggered(bool bEn_)
{
    CSchemGraphicsScene* pScene = static_cast<CSchemGraphicsScene*>(ui->gvSchem->scene());
    assert(pScene);

    pScene->SetMode(bEn_ ? CSchemGraphicsScene::eM_Adding : CSchemGraphicsScene::eM_Empty);
    if(bEn_)
    {
        ui->swSettings->setCurrentIndex(1);
        pScene->SetElementCreator(CElemetManager::GetInstance().GetElementCreator("IC"));
    }
}

void SchemEditor::actEditLink_triggered(bool bEn_)
{
    ui->swSettings->setCurrentIndex(2);

    CSchemGraphicsScene* pScene = static_cast<CSchemGraphicsScene*>(ui->gvSchem->scene());
    if(pScene)
    {
        // Режим редактирования связей
        pScene->SetMode(bEn_ ? CSchemGraphicsScene::eM_EditLink : CSchemGraphicsScene::eM_Empty);
    }
}

void SchemEditor::actHand_triggered(bool bEn_)
{
    ui->swSettings->setCurrentIndex(0); // Режим параметров листа
    ui->gvSchem->EnableDragMode(bEn_);
}

void SchemEditor::actMove_triggered(bool bEn_)
{
    ui->swSettings->setCurrentIndex(0); // Режим параметров листа

    CSchemGraphicsScene* pScene = static_cast<CSchemGraphicsScene*>(ui->gvSchem->scene());
    if(pScene)
        pScene->SetMode(bEn_ ? CSchemGraphicsScene::eM_MoveItem : CSchemGraphicsScene::eM_Empty);
}

void SchemEditor::actDelete_toggled(bool bEn_)
{
    ui->swSettings->setCurrentIndex(0); // Режим параметров листа

    CSchemGraphicsScene* pScene = static_cast<CSchemGraphicsScene*>(ui->gvSchem->scene());
    if(pScene)
        pScene->SetMode(bEn_ ? CSchemGraphicsScene::eM_Delete : CSchemGraphicsScene::eM_Empty);
}

//========================================================================================

// Регистрация креатора элемента
namespace
{
    bool bOk = CElemetManager::GetInstance().RegisterElement("IC", CElement::Create);
}

CElement::CElement(QGraphicsItem *pParent_)
    : QGraphicsItem(pParent_)
    , m_nSize(8)
{
    setFlags(flags() | ItemIsMovable);
    setData(c_nTypeRole, eT_Schem);
}

void CElement::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    paintScheme(painter, m_nSize);
}

QRectF CElement::boundingRect() const
{
    return QRectF(mmToPx(-c_fBoundPadR),
                  mmToPx(-c_fBoundPadR),
                  mmToPx(4 * c_fGrid + 2 * c_fBoundPadR),
                  mmToPx((m_nSize - 1) * c_fGrid + 2 * c_fBoundPadR));
}

void CElement::SetSize(uint8_t nSize_)
{
    m_nSize = nSize_;
}

QGraphicsItem *CElement::Create()
{
    QGraphicsItem* pItem = new CElement(nullptr);
    return pItem;
}

void CElement::paintScheme(QPainter *pPainter_, int nSize_)
{
    pPainter_->save();

    pPainter_->setPen(c_elementPen);
    pPainter_->drawRect(mmToPx(c_fPadR * 2),
                        mmToPx(-c_fPadR),
                        mmToPx(c_fGrid * 3),
                        mmToPx(c_fGrid * (nSize_ - 1) + 2 * c_fPadR));

   for(int i = 0; i < nSize_; i++)
   {
       paintPad(pPainter_, QPointF(0, mmToPx(i * c_fGrid)));
       paintPad(pPainter_, QPointF(mmToPx(4 * c_fGrid), mmToPx(i * c_fGrid)));
   }

   pPainter_->restore();
}

void CElement::paintPad(QPainter* pPainter_, const QPointF& point_)
{
    pPainter_->setPen(QPen(Qt::cyan, 0));
    pPainter_->setBrush(Qt::cyan);
    pPainter_->drawEllipse(point_, mmToPx(c_fPadR), mmToPx(c_fPadR));

    pPainter_->setPen(QPen(Qt::black, 0));
    pPainter_->setBrush(Qt::black);
    pPainter_->drawEllipse(point_, mmToPx(c_fPadR / 2), mmToPx(c_fPadR / 2));
}

//========================================================================================
CSchemGraphicsScene::CSchemGraphicsScene(QObject *pParent_)
    : QGraphicsScene(pParent_)
    , m_routePen(c_linkPen)
    , m_mode(eM_MoveItem)
    , m_pVisualRect(nullptr)
    , m_pLinkLine(nullptr)
    , m_pCurrentLink(nullptr)
    , m_pMovableItem(nullptr)
{
    // Ширина проводника
    SetRouteWidth(1);

    // Цвет сцены
    QBrush sceneBrush(QBrush(QColor("#FFF9A3")));
    setBackgroundBrush(sceneBrush);

    // Ограничивающий прямоугольник
    m_pVisualRect = new QGraphicsRectItem;
    m_pVisualRect->setBrush(Qt::NoBrush);// Без заливки
    QPen vRectPen(QColor("#5852FF"));
    vRectPen.setWidth(5);
    vRectPen.setCosmetic(true);
    m_pVisualRect->setPen(vRectPen);     // Синяя рамка
    addItem(m_pVisualRect);
}

void CSchemGraphicsScene::SetPage(const QSizeF &size_)
{
    if(m_pageSize != size_)
    {
        m_pageSize = size_;
        float w = mmToPx(m_pageSize.width());
        float h = mmToPx(m_pageSize.height());

        // Изменить размер сцены
        // Рзамер прямоугольника + 10%
        QRectF sr(0, 0, w + w / 10, h + h / 10);
        setSceneRect(sr);

        // Изменить размер ограничивающего прямоугольника
        QRectF rect(sr.center().x() - w / 2 , sr.center().y() - h / 2, w, h);
        m_pVisualRect->setRect(rect);
    }
}

void CSchemGraphicsScene::SetMode(CSchemGraphicsScene::EMode mode_)
{
    // Выход из режима редактирования связей
    if(m_mode == eM_EditLink && mode_ != eM_EditLink)
    {
        if(m_pCurrentLink)
        {
            removeItem(m_pCurrentLink);
            m_pCurrentLink = nullptr;
        }

        if(m_pLinkLine)
        {
            removeItem(m_pLinkLine);
            m_pLinkLine = nullptr;
        }
    }

    // Выход из режима добавления элементов
    if(m_mode == eM_Adding && mode_ != eM_Adding)
    {
        removeItem(m_pMovableItem);
        m_pMovableItem = nullptr;
    }

    m_mode = mode_;
}

void CSchemGraphicsScene::ExportSchem(SSchem &schem_) const
{
    schem_.m_vecElements.clear();
    schem_.m_vecLinks.clear();
    schem_.m_sheetSize = GetPageSize();

    for(const auto pItem : items())
    {
        bool bOk = false;
        int type = pItem->data(c_nTypeRole).toInt(&bOk);
        if(!bOk)
            continue;
        if(type == eT_Link ) // связь
        {
            QGraphicsItemGroup* pLinkItem = static_cast<QGraphicsItemGroup*>(pItem);
            SSchem::TLink link;
            for(QGraphicsItem* pLineItem : pLinkItem->childItems())
            {
                QGraphicsLineItem* pSubLineItem = static_cast<QGraphicsLineItem*>(pLineItem);
                SLinkLine line;
                line.m_nLayerNumber = 0; // слой
                line.m_line = pSubLineItem->line(); //линия
                line.m_fWidth = pSubLineItem->pen().widthF(); //ширина
                link.push_back(line);
            }
            schem_.m_vecLinks.push_back(link);
        }
        else if(type == eT_Schem) // элементы
        {
            CElement* pSchemItem = static_cast<CElement*>(pItem);
            SSchem::TElement element;
            element.first = pSchemItem->pos();       // позиция
            element.second = pSchemItem->GetSize();  // размер корпуса
            schem_.m_vecElements.push_back(element);
        }
    }
}

void CSchemGraphicsScene::ImportSchem(const SSchem &schem_)
{
    Clear();

    // размер монтажного пространства
    SetPage(schem_.m_sheetSize);

    // количество слоев
    int nLayers = SSchem::GetSchemLayers(schem_);
    std::vector<QColor> vecPalette;
    GeneratePalette(vecPalette, nLayers, 127);

    // элементы
    for(const auto& element: schem_.m_vecElements)
    {
        CElement* pElementItem = new CElement; // TODO создание через креатор
        pElementItem->setPos(element.first);
        pElementItem->SetSize(element.second);
        addItem(pElementItem);
    }

    // cвязи
    for(const SSchem::TLink& link : schem_.m_vecLinks)
    {
        QGraphicsItemGroup* pLink = new QGraphicsItemGroup;
        pLink->setData(c_nTypeRole, (int)eT_Link);
        for(const SLinkLine& line : link)
        {
            assert(line.m_nLayerNumber < nLayers);

            QGraphicsLineItem* pLine = new QGraphicsLineItem(line.m_line); //линия
            QPen pen(c_linkPen);
            pen.setColor(vecPalette[line.m_nLayerNumber]); // слой
            pen.setWidthF(line.m_fWidth); //ширина
            pLine->setData(c_nLinkLayerRole, line.m_nLayerNumber);
            pLine->setZValue(line.m_nLayerNumber);
            pLine->setPen(pen);
            pLink->addToGroup(pLine);
        }
        addItem(pLink);
    }
}

void CSchemGraphicsScene::SetElementCreator(CElemetManager::TPItemCreator pCreator_)
{
    m_pCreator = pCreator_;
}

float CSchemGraphicsScene::GetRouteWidth() const
{
    return pxToMm(m_routePen.widthF());
}

void CSchemGraphicsScene::Clear()
{
    QList<QGraphicsItem*> listItems = items();
    for(QGraphicsItem* pItem : listItems)
    {
        bool bOk = true;
        int type = pItem->data(c_nTypeRole).toInt(&bOk);
        // Удалять только элементы и связи
        if(bOk && (type == eT_Link || type == eT_Schem))
            removeItem(pItem);
    }
}

void CSchemGraphicsScene::SetRouteWidth(double fWidth_)
{
    m_routePen.setWidthF(mmToPx(fWidth_));

    // Обновить перо редактируемой линии
    if(m_pLinkLine)
        m_pLinkLine->setPen(m_routePen);
}

void CSchemGraphicsScene::mousePressEvent(QGraphicsSceneMouseEvent *e_)
{
    // Привязываем точку к сетке
    e_->setScenePos(bindToGrid(e_->scenePos()));

    // Есть добавляемый элемент
    if(m_mode == eM_Adding && m_pMovableItem)
    {
        m_pMovableItem = nullptr;       // отпускам элемент
        makeElement();                  // создаем новый
        m_pMovableItem->setPos(e_->scenePos());
    }

    // Удаление элементов
    if(m_mode == eM_Delete)
    {
        QGraphicsItem* pItem = itemAt(e_->scenePos(), QTransform());

        bool bOk;
        int type = pItem->data(c_nTypeRole).toInt(&bOk);
        if(bOk)
        {
            if(type == eT_Schem)
                removeItem(pItem);
            else if(type == eT_Link)
            {
                // Проверям касается ли хотябы одна линия мышки
                QGraphicsItemGroup* pLinkItem = static_cast<QGraphicsItemGroup*>(pItem);
                for(QGraphicsItem* pLineItem : pLinkItem->childItems())
                    if(pLineItem->isUnderMouse()) //если да - удалям всю группу
                        removeItem(pItem);
            }
        }
    }

    // Рисование линий
    if(m_mode == eM_EditLink)
    {
        // левая кнопка
        if(e_->button() == Qt::MouseButton::LeftButton)
        {
            if(!m_pCurrentLink)
            {
                m_pCurrentLink = new QGraphicsItemGroup;
                m_pCurrentLink->setData(c_nTypeRole, (int)eT_Link);
                addItem(m_pCurrentLink);
            }

            if(!m_pLinkLine)
            {
                QPointF point = e_->scenePos();
                m_pLinkLine = new QGraphicsLineItem(QLineF(point, point));
                m_pLinkLine->setPen(m_routePen);
                addItem(m_pLinkLine);
            }
            else
            {
                // Копируем построенный отрезок в текущую редактируемую линию
                QGraphicsLineItem* pNewLine = new QGraphicsLineItem(m_pLinkLine->line());
                pNewLine->setPen(m_routePen);
                m_pCurrentLink->addToGroup(pNewLine);

                // Задаем начальные координаты нового отрезка в конце построенного
                QPointF point = m_pLinkLine->line().p2();
                m_pLinkLine->setLine(QLineF(point, point));
            }
        }

        // правая кнопка
        if(e_->button() == Qt::MouseButton::RightButton)
        {
            // Если построенная линия пустая
            if(m_pCurrentLink->childItems().isEmpty())
                removeItem(m_pCurrentLink); // стираем построеную линию
             m_pCurrentLink = nullptr;      // иначе проста прекращаем редактирование

            removeItem(m_pLinkLine);         // удаляем текущий отрезок
            m_pLinkLine = nullptr;
        }
    }

    QGraphicsScene::mousePressEvent(e_);
}

void CSchemGraphicsScene::mouseMoveEvent(QGraphicsSceneMouseEvent *e_)
{
    // Привязываем точку к сетке
    e_->setScenePos(bindToGrid(e_->scenePos()));

    // Есть добавляемый элемент
    if(m_mode == eM_Adding)
    {
        if(!m_pMovableItem) // если нету - создадим
            makeElement();
        m_pMovableItem->setPos(e_->scenePos());
    }

    // перемещение компонента
    if(m_mode == eM_MoveItem)
       QGraphicsScene::mouseMoveEvent(e_);

    // редактирование текущего отрезка линии
    if(m_mode == eM_EditLink && m_pLinkLine)
    {
        // Только ортогональное проведение проводников
        QPointF delta = e_->scenePos();
        delta -= m_pLinkLine->line().p1();
        QPointF point;
        if(fabs(delta.x()) < fabs(delta.y()))
            point = QPointF(m_pLinkLine->line().p1().x(), e_->scenePos().y());
        else
            point = QPointF(e_->scenePos().x(), m_pLinkLine->line().p1().y());

        // Обновляем координаты
        QLineF newLine(m_pLinkLine->line().p1(), point);
        m_pLinkLine->setLine(newLine);
    }
}

void CSchemGraphicsScene::mouseReleaseEvent(QGraphicsSceneMouseEvent *e_)
{
    // Привязываем точку к сетке
    e_->setScenePos(bindToGrid(e_->scenePos()));

    if(m_mode == eM_MoveItem)
        QGraphicsScene::mouseReleaseEvent(e_);
}

QPointF CSchemGraphicsScene::bindToGrid(const QPointF &point_)
{
    // Получить координаты в делениях сетки
    int nCol = int(point_.x() / mmToPx(c_fGrid));
    int nRow = int(point_.y() / mmToPx(c_fGrid));

    // рассмотрим расстояние до соседних ячеек
    typedef std::pair<float, QPointF> TPoint;
    typedef std::vector<TPoint> TDistPoints;     // квадрат расстояния до точки, координаты
    TDistPoints points;

    // центральная
    QPointF cp(nCol * mmToPx(c_fGrid), nRow * mmToPx(c_fGrid));
    points.push_back(std::make_pair(SquareDistance(point_, cp), cp));
    // слева
    QPointF lp(cp.x() - mmToPx(c_fGrid), cp.y());
    points.push_back(std::make_pair(SquareDistance(point_, lp), lp));
    // справа
    QPointF rp(cp.x() + mmToPx(c_fGrid), cp.y());
    points.push_back(std::make_pair(SquareDistance(point_, rp), rp));
    // сверху
    QPointF tp(cp.x(), cp.y() - mmToPx(c_fGrid));
    points.push_back(std::make_pair(SquareDistance(point_, tp), tp));
    // снизу
    QPointF bp(cp.x(), cp.y() + mmToPx(c_fGrid));
    points.push_back(std::make_pair(SquareDistance(point_, bp), bp));

    // получить точку с ниименьшим квадратом расстояния
    QPointF point = std::min_element(points.begin(), points.end(), [](const TPoint& p1_, const TPoint& p2_)
    { return p1_.first < p2_.first; })->second;

//    return cp;
    return point;
}

void CSchemGraphicsScene::makeElement()
{
    if(m_pCreator)
    {
        m_pMovableItem = (*m_pCreator)();  // Создаем новый
        assert(m_pMovableItem);
        addItem(m_pMovableItem);           // Добавляем на сцену
    }
    else
        removeItem(m_pMovableItem);
}

//========================================================================================
void CSchemGraphicsView::EnableDragMode(bool bIsEnabled_)
{
    setDragMode(bIsEnabled_ ? ScrollHandDrag : NoDrag);
}

void CSchemGraphicsView::wheelEvent(QWheelEvent *e_)
{
    QPointF newCenterPoint = mapToScene(e_->pos());
    scaledBy(std::pow(4.0 / 3.0, (e_->delta() / 240.0)));
    centerOn(newCenterPoint);
}

void CSchemGraphicsView::mousePressEvent(QMouseEvent *e_)
{
    QGraphicsView::mousePressEvent(e_);
}

void CSchemGraphicsView::mouseMoveEvent(QMouseEvent *e_)
{
    QGraphicsView::mouseMoveEvent(e_);
}

void CSchemGraphicsView::mouseReleaseEvent(QMouseEvent *e_)
{
    QGraphicsView::mouseReleaseEvent(e_);
}
