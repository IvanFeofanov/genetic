#ifndef SCHEMTOGRAPHBUILDER_H
#define SCHEMTOGRAPHBUILDER_H

#include <vector>

#include <qrect.h>

#include "graph.h"
#include "basetypes.h"

/// @brief Класс алгоритма построения графа по линиям связей
class CSchemToGraphBuilder
{
public:
    /// @brief Создание графа по массиву связей
    /// @param links_ вектор связей схемы
    /// @param graph_ результирующий граф
    static void MakeGraph(const std::vector<SSchem::TLink>& links_, CNonDirGraph& graph_);

private:
    /// Проверка двух связей на пересечение
    static bool checkLinkForIntersection(const SSchem::TLink& link1_, const SSchem::TLink& link2_);

    /// Проверка прямых на пересечение
    static bool checkLineForItersection(const SLinkLine& line1_, const SLinkLine& line2_);

    /// Вычислить ограничивающий прямоугольник для линии
    static QRectF calcBoundingRectForLine(const SLinkLine& line_);
};

/// @brief Класс алгоритмов раскраски графа
class CColorizer
{
public:
    /// Раскраска графа методом A1
    static void MethodA1(CNonDirGraph& graph_);

    /// Раскраска графа методом A2
    static void MethodA2(CNonDirGraph& graph_);

    /// Распределение связей по слоям в соответсвии с графом
    static void DistByLayers(const CNonDirGraph& graph_, SSchem& schem_);
};

#endif // SCHEMTOGRAPHBUILDER_H