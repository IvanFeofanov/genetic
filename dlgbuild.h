#ifndef DLGBUILD_H
#define DLGBUILD_H

#include <QDialog>
#include "basetypes.h"
#include "graph.h"

namespace Ui {
class dlgBuild;
}

/// @brief Класс диалога раскраски
class CDlgBuild : public QDialog
{
    Q_OBJECT

public:
    explicit CDlgBuild(QWidget *pParent_ = 0);
    ~CDlgBuild();

    /// Установить текущую схему
    /// @param schem_ - схема
    void SetSchem(const SSchem& schem_);

    /// вернуть раскрашенную схему
    /// @return ссылка на раскрашенную схему
    const SSchem& GetColorizedSchem() const { return m_schem; }

signals:
    /// раскраска закончена
    void buildFinished();

private slots:
    /// Построение
    void build();

private:
    Ui::dlgBuild*   m_pUi;      ///< Ui
    CNonDirGraph    m_graph;    ///< Текущий граф
    SSchem          m_schem;    ///< Текущая схема
};

#endif // DLGBUILD_H
