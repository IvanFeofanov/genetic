#include "graph.h"

#include "assert.h"
#include <algorithm>

///======================================================

//////////////////////////////////////////////////////////
///
CVertex::CVertex(TId nId_ /*=0*/)
{
    m_color = -1;
    m_id = nId_;
}

//////////////////////////////////////////////////////////
///
void CVertex::SetColor(int nColor_)
{
    m_color = nColor_;
}


///======================================================

//////////////////////////////////////////////////////////
///
CNonDirGraph::CNonDirGraph()
{

}

//////////////////////////////////////////////////////////
///
void CNonDirGraph::Clear()
{
    m_vertexes.clear();
}

//////////////////////////////////////////////////////////
///
void CNonDirGraph::Bleach()
{
    for(auto& v : m_vertexes)
        v.second.SetColor(-1);
}

//////////////////////////////////////////////////////////
///
void CNonDirGraph::InitVertexes(int nCount_)
{
    Clear();
    for(int i = 0; i < nCount_; i++)
        m_vertexes.insert(std::make_pair(i, CVertex(i)));
}

//////////////////////////////////////////////////////////
///
const CVertex &CNonDirGraph::AddVertex()
{
    CVertex::TId id = m_vertexes.size();
    CVertex vertex(id);
    m_vertexes.insert(std::make_pair(id, vertex));
    return m_vertexes[id];
}

//////////////////////////////////////////////////////////
///
void CNonDirGraph::RemoveVertex(CVertex::TId vertexId_)
{
    TMapVertex::const_iterator itVertex = m_vertexes.find(vertexId_);
    assert(itVertex != m_vertexes.cend());

    // Удалить вершину из списков смежных вершин
    for(CVertex::TId id : itVertex->second.m_adj)
    {
        TMapVertex::const_iterator itId = m_vertexes.find(id);
        assert(itId != m_vertexes.cend());

        CVertex::TSetAdj& adj = m_vertexes[id].m_adj;
        CVertex::TSetAdj::iterator it = adj.find(vertexId_);
        assert(it != adj.end());
        adj.erase(it);
    }

    // Удалить вершину из списка вершин
    m_vertexes.erase(itVertex);
}

//////////////////////////////////////////////////////////
///
const CVertex &CNonDirGraph::GetVertex(CVertex::TId vertexId_) const
{
    TMapVertex::const_iterator itVertex = m_vertexes.find(vertexId_);
    assert(itVertex != m_vertexes.cend());
    return itVertex->second;
}

//////////////////////////////////////////////////////////
///
CVertex &CNonDirGraph::GetVertex(CVertex::TId vertexId_)
{
    TMapVertex::iterator itVertex = m_vertexes.find(vertexId_);
    assert(itVertex != m_vertexes.cend());
    return itVertex->second;
}

//////////////////////////////////////////////////////////
///
CNonDirGraph::TVecVertex CNonDirGraph::GetVertexes() const
{
    TVecVertex vecVertex;
    vecVertex.reserve(m_vertexes.size());
    for(const auto& v : m_vertexes)
        vecVertex.push_back(v.second);
    return vecVertex;
}

//////////////////////////////////////////////////////////
///
int CNonDirGraph::GetVertexCount() const
{
    return m_vertexes.size();
}

//////////////////////////////////////////////////////////
///
int CNonDirGraph::GetChromatic() const
{
    std::set<int> setColors;
    for(const auto& v : m_vertexes)
        setColors.insert(v.second.GetColor());

    return std::max((int)setColors.size(), 1);
}

//////////////////////////////////////////////////////////
///
void CNonDirGraph::SetAdj(CVertex::TId vertex2Id_, CVertex::TId vertex1Id_)
{
//    assert(vertex1Id_ < m_vertexes.size());
//    assert(vertex2Id_ < m_vertexes.size());

//    m_vertexes[vertex1Id_].m_adj.insert(vertex2Id_);
//    m_vertexes[vertex2Id_].m_adj.insert(vertex1Id_);

    TMapVertex::iterator itVertex1 = m_vertexes.find(vertex1Id_);
    assert(itVertex1 != m_vertexes.cend());
    TMapVertex::iterator itVertex2 = m_vertexes.find(vertex2Id_);
    assert(itVertex2 != m_vertexes.cend());

    itVertex1->second.m_adj.insert(vertex2Id_);
    itVertex2->second.m_adj.insert(vertex1Id_);
}
