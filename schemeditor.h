#ifndef SCHEMEDITOR_H
#define SCHEMEDITOR_H

#include <QWidget>
#include <QGraphicsItem>
#include <QGraphicsView>
#include <QPrinter>
#include <QWheelEvent>
#include <QActionGroup>
#include <QTreeWidgetItem>

#include <basetypes.h>
#include "elemetmanager.h"

//========================================================================================
namespace Ui {
class SchemEditor;
}

/// @brief класс виджета редактора схемы
class SchemEditor : public QWidget
{
    Q_OBJECT

public:
    explicit SchemEditor(QWidget *parent = 0);
    ~SchemEditor();

    /// Установить режим редактирования
    void setEditable(bool bEnabled_);

    /// Экспортировать схему
    void ExportSchem(SSchem& schem_) const;

    /// Импортировать схему
    void ImportSchem(const SSchem& schem_);

    /// Очистить монтажное пространство
    void Clear();

    /// Нарисовать прямоугольник
    //void DrawRect(const QRectF& rect_);

public slots:
    /// Показать всю схему
    void ZoomAll();

private slots:
    /// Добавление элемента
    void actAddElement_triggered(bool bEn_);

    /// Редактирование линий связей
    void actEditLink_triggered(bool bEn_);

    /// Перемещение сцены
    void actHand_triggered(bool bEn_);

    /// Перемещение элемента
    void actMove_triggered(bool bEn_);

    /// Удаление элементов
    void actDelete_toggled(bool bEn_);

    /// Выбран один из инструментов
    void toolActGroup_triggered(QAction* pAction_);

    /// Изменился виджет параметров
    void swSettings_currentChanged(int nWidget);

    /// Изменились размеры листа
    void dsbSheetWidth_valueChanged(double dW_);
    void dsbSheetHeight_valueChanged(double dH_);

    /// Изменился элемент
    void sbNumberOutputs_valueChanged(int nValue_);

    /// Изменился чекбокс в списке включенных слоев
    void trwLayers_itemChanged(QTreeWidgetItem* pItem_, int nCol_);

private:
    /// Обновить список слоев
    void refresh_trwLayers(const SSchem &schem_);

    /// Фильтрация слоев
    void filterLayers();

    /// Отключить все инструменты
    void disableAllTools();

private:
    enum ESettingsTab
    {
        eSt_Sheet,
        eSt_Element,
        eSt_Route
    };

    Ui::SchemEditor* ui;
    QActionGroup*    m_pToolActGroup;
};

//========================================================================================

/// @brief класс графического представления электронного компонента
class CElement : public QGraphicsItem
{
public:
    CElement(QGraphicsItem* pParent_ = nullptr);

    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    QRectF boundingRect() const;

    /// Установить размер микросхемы
    void SetSize(uint8_t nSize_);

    /// Получить размер схемы
    int GetSize() const { return m_nSize; }

    /// Креатор
    static QGraphicsItem* Create();
private:
    /// Нарисовать микросхему с указанным числом рядов ног
    void paintScheme(QPainter* pPainter_, int nSize_);
    void paintPad(QPainter* pPainter_, const QPointF& point_);

private:
    int m_nSize;
};


//========================================================================================
/// @brief Класс графического представления редактора схемы
class CSchemGraphicsView : public QGraphicsView
{
    Q_OBJECT

public:
    explicit CSchemGraphicsView(QWidget* pParent_) : QGraphicsView(pParent_)
    {
        setMouseTracking(true);
    }

public slots:
    /// Включить возможность перемещения сцены мышкой
    void EnableDragMode(bool bIsEnabled_);

protected:
    void wheelEvent(QWheelEvent* e_);
    void mousePressEvent(QMouseEvent* e_);
    void mouseMoveEvent(QMouseEvent* e_);
    void mouseReleaseEvent(QMouseEvent* e_);

private:
    void scaledBy(double dFactor_) { scale(dFactor_, dFactor_); }
};


//========================================================================================
/// @brief класс сцены редактора схемы
class CSchemGraphicsScene : public QGraphicsScene
{
    Q_OBJECT

public:
    /// Режимы редактирования
    enum EMode
    {
        eM_Empty,       /// Не активен
        eM_MoveItem,    /// Перемещение элементов
        eM_EditLink,    /// Редактирование связей
        eM_Adding,      /// Добавление компонетов
        eM_Delete       /// Удаление
    };

    explicit CSchemGraphicsScene(QObject* pParent_);

    /// Получить размер страницы
    /// @return Размер в mm
    const QSizeF& GetPageSize() const { return m_pageSize; }

    /// Получить прямоугольник страницы
    /// @return прямоугольник страницы
    QRectF GetPageRect() const { return m_pVisualRect->boundingRect(); }

    /// Установить режим редактирования
    void SetMode(EMode mode_);

    /// Получить режим редактирования
    EMode GetMode() const { return m_mode; }

    /// Экспортировать схему
    void ExportSchem(SSchem& schem_) const;

    /// Импортировать схему
    void ImportSchem(const SSchem& schem_);

    /// Установить креатор элемента
    void SetElementCreator(CElemetManager::TPItemCreator pCreator_);

    /// Получить ширину проводника
    /// @return ширина проводника в мм
    float GetRouteWidth() const;

public slots:
    /// Очистить монтажное пространство
    void Clear();

    /// Установить страницу
    /// @param size_ Размер в mm
    void SetPage(const QSizeF& size_);

    /// Установить ширину проводника
    /// @param fWidth_ ширина проводника в mm
    void SetRouteWidth(double fWidth_);

protected:
    /// Событие нажатия кнопки мыши
    void mousePressEvent(QGraphicsSceneMouseEvent* e_) override;
    /// Событие перемещения мыши
    void mouseMoveEvent(QGraphicsSceneMouseEvent* e_) override;
    /// Событие отпускания кнопки мыши
    void mouseReleaseEvent(QGraphicsSceneMouseEvent* e_) override;

protected:
    /// Привязать точку к сетке
    /// @param point_ точка
    /// @return позиция точки на сетке
    static QPointF bindToGrid(const QPointF& point_);

    /// Сделать новый элемент
    void makeElement();
private:
    QPen                          m_routePen;   ///< Ширина проводника
    CElemetManager::TPItemCreator m_pCreator;   ///< Креатор элементов

    QSizeF              m_pageSize;     ///< Размер страницы
    EMode               m_mode;         ///< Режим редактирования

    QGraphicsRectItem*  m_pVisualRect;  ///< Ограничивающий прямоугольник
    QGraphicsLineItem*	m_pLinkLine;	///< Редактируемая часть связи
    QGraphicsItemGroup* m_pCurrentLink; ///< Текущая редактируемая связь
    QGraphicsItem*      m_pMovableItem; ///< Текущий перемещаемый элемент (при добавлении)
};

#endif // SCHEMEDITOR_H
