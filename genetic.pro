#-------------------------------------------------
#
# Project created by QtCreator 2018-03-19T15:07:42
#
#-------------------------------------------------

QT       += core gui printsupport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = genetic
TEMPLATE = app

CONFIG += c++11
CONFIG += c++14

RESOURCES += ico.qrc

SOURCES += main.cpp\
        mainwindow.cpp \
    graph.cpp \
    schemeditor.cpp \
    elemetmanager.cpp \
    graphviewer.cpp \
    basealgorithms.cpp \
    schemtographbuilder.cpp \
    dlgbuild.cpp

HEADERS  += mainwindow.h \
    graph.h \
    schemeditor.h \
    basetypes.h \
    elemetmanager.h \
    graphviewer.h \
    basealgorithms.h \
    schemtographbuilder.h \
    dlgbuild.h

FORMS    += mainwindow.ui \
    schemeditor.ui \
    graphviewer.ui \
    dlgbuild.ui
