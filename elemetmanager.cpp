#include "elemetmanager.h"

CElemetManager &CElemetManager::GetInstance()
{
    static CElemetManager instance;
    return instance;
}

bool CElemetManager::RegisterElement(const std::string &strName_, CElemetManager::TPItemCreator pCreator_)
{
    m_mapCreators.insert(std::make_pair(strName_, pCreator_));
    return true;
}

CElemetManager::TPItemCreator CElemetManager::GetElementCreator(const std::string &strName_) const
{
    TMapCreators::const_iterator itFind = m_mapCreators.find(strName_);
    if(itFind != m_mapCreators.cend())
        return itFind->second;

    return nullptr;
}

CElemetManager::TVecNames CElemetManager::GetAllElementNames() const
{
    TVecNames names;
    for(const auto& element : m_mapCreators)
        names.push_back(element.first);
    return names;
}
