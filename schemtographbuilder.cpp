#include "schemtographbuilder.h"

#include <qrect.h>

#include <assert.h>

void CSchemToGraphBuilder::MakeGraph(const std::vector<SSchem::TLink> &links_, CNonDirGraph &graph_)
{
    // Создать граф с количество вершин равным количеству связе в схеме
    graph_.InitVertexes(links_.size());

    // Проверям на пересечение кадждую связь с каждой связью
    for(int i = 1; i < links_.size(); i++)
        for(int j = 0; j < i; j++)
        {
            // Если связи пересекаются - делаем соответсвующие вершины графа смежными
            if(checkLinkForIntersection(links_[i], links_[j]))
                graph_.SetAdj(i, j);
        }
}

bool CSchemToGraphBuilder::checkLinkForIntersection(const SSchem::TLink &link1_, const SSchem::TLink &link2_)
{
    // Проверяем пересекается ли хотыбы один сегмент i-той связи
    // хотябы с одним сегментом j-той связи
    for(const SLinkLine& line1 : link1_)
        for(const SLinkLine& line2 : link2_)
            if(checkLineForItersection(line1, line2))
                return true;

    // Нет пересекающихся сегментов связей
    return false;
}

bool CSchemToGraphBuilder::checkLineForItersection(const SLinkLine &line1_, const SLinkLine &line2_)
{
    QRectF bound1 = calcBoundingRectForLine(line1_);
    QRectF bound2 = calcBoundingRectForLine(line2_);

    // Вычисление признака пересечения прямоугольника с использованием теоремы о разделяющей оси
    bool bintersect = !(bound1.top() > bound2.bottom() || bound1.bottom() < bound2.top() ||
                        bound1.left() > bound2.right() || bound1.right() < bound2.left());

    return bintersect;
}

QRectF CSchemToGraphBuilder::calcBoundingRectForLine(const SLinkLine &line_)
{
    double dx = std::fabs(line_.m_line.x1() - line_.m_line.x2());
    double dy = std::fabs(line_.m_line.y1() - line_.m_line.y2());

    QPointF topLeft;
    QPointF bottomRight;

    // Если линия расположе вертикально
    if(dx < std::numeric_limits<double>::epsilon())
    {
        std::pair<double, double> minmaxY = std::minmax(line_.m_line.y1(), line_.m_line.y2());

        topLeft = QPointF(line_.m_line.p1().x() - line_.m_fWidth / 2, minmaxY.first + line_.m_fWidth / 2);
        bottomRight = QPointF(line_.m_line.p2().x() + line_.m_fWidth / 2, minmaxY.second + line_.m_fWidth / 2);
    }
    else if(dy < std::numeric_limits<double>::epsilon()) // горизонтально
    {
        std::pair<double, double> minmaxX = std::minmax(line_.m_line.x1(), line_.m_line.x2());

        topLeft = QPointF(minmaxX.first + line_.m_fWidth / 2, line_.m_line.p1().y() - line_.m_fWidth / 2);
        bottomRight = QPointF(minmaxX.second + line_.m_fWidth / 2, line_.m_line.p2().y() + line_.m_fWidth / 2);
    }
    else
        assert(false);

    QRectF bound;
    bound.setTopLeft(topLeft);
    bound.setBottomRight(bottomRight);
    return bound;
}


void CColorizer::MethodA1(CNonDirGraph &graph_)
{
    // Обесцветить граф
    graph_.Bleach();

    // Получить копию массива вершин
    CNonDirGraph::TVecVertex srcVertexes = graph_.GetVertexes();

    // Упорядочить вершины по степеням вершин
    auto pred = [](const CVertex& v1_, const CVertex& v2_)
    {
        return v1_.GetAdjVertex().size() > v2_.GetAdjVertex().size();
    };
    std::sort(srcVertexes.begin(), srcVertexes.end(), pred);

    // Уже окрашенные исходные вершины
    std::set<CVertex::TId> setSrcAlready;
    // Начальный цвет
    int nColor = 0;

    // Пока все вершины не окрашены
    while(true)
    {
        // Уже окрашенные в текущий цвет вершины
        CNonDirGraph::TVecVertex vecAlready;

        // Все вершины окрашены
        bool bEnd = true;

        // Для каждой исходной вершины
        for(CVertex& sv : srcVertexes)
        {
            // Текущая исходная вершина уже окрашена
            std::set<CVertex::TId>::const_iterator itFind = setSrcAlready.find(sv.GetId());
            if(itFind != setSrcAlready.cend())
                continue;   // Пропускаем

            // Еще не все вершины окрашены
            bEnd = false;

            // Cвязана ли хотябы одна из уже окрашенных вершин с текущей исходной
            bool bConnected = false;
            for(const CVertex& av : vecAlready)
            {
                CVertex::TSetAdj::const_iterator itFind =
                        av.GetAdjVertex().find(sv.GetId());
                if(itFind != av.GetAdjVertex().cend())
                {
                    bConnected = true; break;
                }
            }

            // Если не связана
            if(!bConnected)
            {
                // окрашиваем в текущий
                sv.SetColor(nColor);
                // добавляем в список орашенных в текущий цвет
                vecAlready.push_back(sv);
                // добавляем в множество уже окрашенных
                setSrcAlready.insert(sv.GetId());
            }
            // Иначе - пропускаем
        }

        // все вершины окрашены
        if(bEnd)
            break;

        // Следующий цвет
        nColor++;
    }

    // Перенос цвета в исходный граф
    for(const CVertex& v : srcVertexes)
        graph_.GetVertex(v.GetId()).SetColor(v.GetColor());
}

void CColorizer::MethodA2(CNonDirGraph& graph_)
{
    // Обесцветить граф
    graph_.Bleach();

    // Уже окрашенные исходные вершины
    std::set<CVertex::TId> setSrcAlready;

    // пока есть не окрашенные вершины
    for(int nColor = 0; setSrcAlready.size() != graph_.GetVertexCount(); nColor++)
    {
        // Получить копию исходного графа
        CNonDirGraph srcGraph = graph_;
        // Удалить уже окрашенные вершины
        for(CVertex::TId id : setSrcAlready)
            srcGraph.RemoveVertex(id);

        while(true)
        {
            // Выбрать вершину с максимальной локальной степенью
            CNonDirGraph::TMapVertex::const_iterator it = std::max_element(srcGraph.GetMapVertexes().begin(),
            srcGraph.GetMapVertexes().end(), [](const auto& v1_, const auto& v2_) -> bool
            {
                return v1_.second.GetAdjVertex().size() < v2_.second.GetAdjVertex().size();
            });

            // если множество ребер равно нулю (отсутсвуют смежные вершины)
            if(it->second.GetAdjVertex().empty())
                break;  // закончить удаление ребер

            // иначе - удалить вершину
            srcGraph.RemoveVertex(it->second.GetId());
        }

        for(const CVertex& vertex : srcGraph.GetVertexes())
        {
            // Окрасить оставшиеся вершины в текущий цвет
            graph_.GetVertex(vertex.GetId()).SetColor(nColor);
            // Запомнить уже окрашенные вершины
            setSrcAlready.insert(vertex.GetId());
        }
    }
}

void CColorizer::DistByLayers(const CNonDirGraph &graph_, SSchem &schem_)
{
    // Для каждой линии связи
    for(int i = 0; i < schem_.m_vecLinks.size(); i++)
        for(SLinkLine& line : schem_.m_vecLinks[i])
            line.m_nLayerNumber = graph_.GetVertex(i).GetColor();
}
