#ifndef GRAPH_H
#define GRAPH_H

#include <set>
#include <vector>
#include <map>

////////////////////////////////////////////////
/// \brief Класс вершины графа
///
class CVertex
{
public:
    typedef unsigned int    TId;      /// Тип идентификатора вершины
    typedef std::set<TId>   TSetAdj;  /// Тип множества смежных вершин

    /// Конструктор
    CVertex(TId nId_ = 0);

    /// Получить идентификатор вершины
    TId GetId() const { return m_id; }

    /// Получить цвет вершины
    int GetColor() const { return m_color; }

    /// Установить цвет вершины
    void SetColor(int nColor_);

    /// Получить множество смежных вершин
    const TSetAdj& GetAdjVertex() const { return m_adj; }

private:
    TId         m_id;       ///< Идентификатор вершины
    int         m_color;    ///< Цвет вершины
    TSetAdj     m_adj;      ///< Смежные вершины

    friend class CNonDirGraph;
};

////////////////////////////////////////////////
/// \brief Класс графа
///
class CNonDirGraph
{
public:
    typedef std::vector<CVertex>   TVecVertex; /// Тип списка вершин
    typedef std::map<CVertex::TId, CVertex> TMapVertex; /// Тип мапа вершин

    /// Конструктор
    CNonDirGraph();

    /// Очистить список вершин
    void Clear();

    /// Обесцветить вершины графа
    void Bleach();

    /// Создать заданное количесво вершин
    void InitVertexes(int nCount_);

    /// Добавить вершину
    const CVertex& AddVertex();

    /// Удалить вершину
    void RemoveVertex(CVertex::TId vertexId_);

    /// Получить вершину
    const CVertex& GetVertex(CVertex::TId vertexId_) const;
    CVertex& GetVertex(CVertex::TId vertexId_);

    /// Получить массив вершин
    TVecVertex GetVertexes() const;

    /// Получить ссылку на мап вершин
    const TMapVertex& GetMapVertexes() const { return m_vertexes; }

    /// Получить количество вершин
    int GetVertexCount() const;

    /// Получить хроматическое число графа
    /// @return хроматическое число 1 - если бесцветный
    int GetChromatic() const;

    /// Сделать вершины смежными
    void SetAdj(CVertex::TId vertex2Id_, CVertex::TId vertex1Id_);

private:
    TMapVertex      m_vertexes; ///< Вершины графа
};

#endif // GRAPH_H
