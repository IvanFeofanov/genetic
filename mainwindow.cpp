#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "schemeditor.h"
#include "schemtographbuilder.h"

#include <QFileDialog>
#include <QXmlStreamWriter>
#include <QXmlStreamReader>
#include <QXmlStreamAttribute>
#include <QTextStream>
#include <QMessageBox>
#include <assert.h>

#include <iostream>
#include <QDebug>

CMainWindow::CMainWindow(QWidget *pParent_)
    : QMainWindow(pParent_)
    , m_pUi(new Ui::CMainWindow)
    , m_pSettings(new QSettings(QApplication::organizationName(), QApplication::applicationName()))
    , m_pDlgBuild(nullptr)
{
    m_pUi->setupUi(this);

    connect(m_pUi->actBuild, SIGNAL(triggered(bool)), this, SLOT(actBuild_triggered(bool)));
    connect(m_pUi->actNew, SIGNAL(triggered(bool)), this, SLOT(actNew_triggered()));
    connect(m_pUi->actOpen, SIGNAL(triggered(bool)), this, SLOT(actOpen_triggered()));
    connect(m_pUi->actSave, SIGNAL(triggered(bool)), this, SLOT(actSave_triggered()));
    connect(m_pUi->actSaveAs, SIGNAL(triggered(bool)), this, SLOT(actSaveAs_triggered()));
    m_pUi->resultSchem->setEditable(false);

    // Путь к текущей плате
    m_sFilePath = m_pSettings->value("project_file_path", "").toString();
}

CMainWindow::~CMainWindow()
{
    delete m_pUi;
}

void CMainWindow::actNew_triggered()
{
    if(QMessageBox::question(this, "Сохранить проект", "Сохранить текущий проект") == QMessageBox::Yes)
        actSave_triggered();

    m_pUi->resultSchem->Clear();
    m_pUi->schemEditor->Clear();
    m_pUi->tabWidget->setCurrentIndex(0);
    m_sFilePath.clear();
}

void CMainWindow::actOpen_triggered()
{
    // Файл
    QString sFileName = QFileDialog::getOpenFileName(this, "Открыть файл платы", m_sFilePath, "*.xml");
    if(sFileName.isEmpty())
        return;

    // текущий проект
    m_sFilePath = sFileName;

    QFile* pFile = new QFile(sFileName);
    if(!pFile->open(QIODevice::ReadOnly | QIODevice::Text))
        return;

    QApplication::setOverrideCursor(Qt::WaitCursor);

    // Документ
    QXmlStreamReader xmlReader(pFile);

    // Читаем схему
    SSchem schem;
    while(!xmlReader.atEnd() && !xmlReader.hasError())
    {
        xmlReader.readNext();
        if(xmlReader.isStartElement())
        {
            // Размер монтажного пространства
            if(xmlReader.name() == "sheet")
            {
                QXmlStreamAttributes attrs = xmlReader.attributes();
                if(attrs.hasAttribute("sheetSize"))
                {
                    float w = 0, h = 0;
                    QString s = attrs.value("sheetSize").toString();
                    QTextStream(&s) >> w >> h;
                    schem.m_sheetSize = QSizeF(w, h);
                }
            }

            // Связи
            if(xmlReader.name() == "link")
            {
                // линии связей
                SSchem::TLink link;
                while(!xmlReader.atEnd() && !xmlReader.hasError() && !(xmlReader.name() == "link" && xmlReader.isEndElement()))
                {
                    xmlReader.readNext();
                    if(xmlReader.isStartElement() && xmlReader.name() == "line")
                    {
                        QXmlStreamAttributes attrs = xmlReader.attributes();
                        if(attrs.hasAttribute("width")
                           && attrs.hasAttribute("layer")
                           && attrs.hasAttribute("line"))
                        {
                            bool bOk = true;
                            SLinkLine line;
                            line.m_fWidth = attrs.value("width").toFloat(&bOk);
                            if(!bOk) continue;

                            line.m_nLayerNumber = attrs.value("layer").toFloat(&bOk);
                            if(!bOk) continue;

                            float x1, y1, x2, y2;
                            QString s = attrs.value("line").toString();
                            QTextStream(&s) >> x1 >> y1 >> x2 >> y2;
                            line.m_line = QLineF(x1, y1, x2, y2);
                            link.push_back(line);
                        }
                    }
                }
                schem.m_vecLinks.push_back(link);
            }

            // Элементы
            if(xmlReader.name() == "element")
            {
                QXmlStreamAttributes attrs = xmlReader.attributes();
                if(attrs.hasAttribute("outputs") && attrs.hasAttribute("position"))
                {
                    bool bOk = true;
                    SSchem::TElement element;
                    element.second = attrs.value("outputs").toInt(&bOk);
                    if(!bOk) continue;

                    float x, y;
                    QString s = attrs.value("position").toString();
                    QTextStream(&s) >> x >> y;
                    element.first = QPointF(x, y);

                    schem.m_vecElements.push_back(element);
                }
            }
        }
    }

    // Ошибки
    if(xmlReader.hasError())
        qDebug() << "XML error: " << xmlReader.errorString() << " " << xmlReader.error();

    // Импортирование
    m_pUi->schemEditor->ImportSchem(schem);

    QApplication::restoreOverrideCursor();
}

void CMainWindow::actSave_triggered()
{
    save(m_sFilePath);
}

void CMainWindow::actSaveAs_triggered()
{
    save("");
}

void CMainWindow::actBuild_triggered(bool bEn_)
{
    // Если диалог не создан - создаем
    if(!m_pDlgBuild)
    {
        m_pDlgBuild = new CDlgBuild(this);
        connect(m_pDlgBuild, SIGNAL(rejected()), m_pUi->actBuild, SLOT(toggle()));
        connect(m_pDlgBuild, SIGNAL(buildFinished()), this, SLOT(colorizeFinished()));
    }

    m_pDlgBuild->setVisible(bEn_);

    // Если диалог окрывается
    if(bEn_)
    {
        // Экспорт исходной платы
        SSchem schem;
        m_pUi->schemEditor->ExportSchem(schem);

        m_pDlgBuild->SetSchem(schem);
    }
}

void CMainWindow::colorizeFinished()
{
    // Вывод платы с разнесенными по слоям линиями связей
    const SSchem& schem = m_pDlgBuild->GetColorizedSchem();
    m_pUi->resultSchem->ImportSchem(schem);
    m_pUi->tabWidget->setCurrentIndex(1); // переключиться на вкладку с результатами
    m_pUi->resultSchem->ZoomAll();        // показать всю плату
}

void CMainWindow::closeEvent(QCloseEvent* pEvent_)
{
    if(QMessageBox::question(this, "Сохранить проект", "Сохранить текущий проект") == QMessageBox::Yes)
        actSave_triggered();

    pEvent_->accept();
}

void CMainWindow::save(const QString &sFilePath_)
{
    // TODO вынести отсюда вызов диалога выбора файла
    if(sFilePath_.isEmpty())
    {
        QString sFileName = QFileDialog::getSaveFileName(this, "Сохранить плату", m_sFilePath, "*.xml");
        if(sFileName.isEmpty())
            return;
        else
            m_sFilePath = sFileName;
    }

    // Экспортируем схему
    SSchem schem;
    m_pUi->schemEditor->ExportSchem(schem);

    // файл
    QFile file(m_sFilePath);
    if(!file.open(QIODevice::WriteOnly))
        return;

    QApplication::setOverrideCursor(Qt::WaitCursor);

    // Документ
    QXmlStreamWriter xmlWriter(&file);
    xmlWriter.setAutoFormatting(true);
    xmlWriter.writeStartDocument();
    xmlWriter.writeStartElement("schem");

    // Параметры листа
    xmlWriter.writeStartElement("sheet");
    xmlWriter.writeAttribute("sheetSize", QString("%1 %2").arg(schem.m_sheetSize.width()).arg(schem.m_sheetSize.height()));
    xmlWriter.writeEndElement();

    // Связи
    xmlWriter.writeStartElement("links");
    for(const SSchem::TLink& link : schem.m_vecLinks)
    {
        // Линии связи
        xmlWriter.writeStartElement(QString("link"));
        for(const SLinkLine& line : link)
        {
            // Линия
            xmlWriter.writeStartElement(QString("line"));
            xmlWriter.writeAttribute("width", QString::number(line.m_fWidth));
            xmlWriter.writeAttribute("layer", QString::number(line.m_nLayerNumber));
            xmlWriter.writeAttribute("line", QString("%1 %2 %3 %4")
                .arg(line.m_line.p1().x()).arg(line.m_line.p1().y()).arg(line.m_line.p2().x()).arg(line.m_line.p2().y()));
            xmlWriter.writeEndElement();
        }
        xmlWriter.writeEndElement(); // \линии
    }
    xmlWriter.writeEndElement(); // \связи

    // Элементы
    xmlWriter.writeStartElement("elements");
    for(const SSchem::TElement& element : schem.m_vecElements)
    {
        // Элемент
        xmlWriter.writeStartElement(QString("element"));
        xmlWriter.writeAttribute("outputs", QString::number(element.second));
        xmlWriter.writeAttribute("position", QString("%1 %2").arg(element.first.x()).arg(element.first.y()));
        xmlWriter.writeEndElement();
    }
    xmlWriter.writeEndElement();

    // Конец документа
    xmlWriter.writeEndElement();
    xmlWriter.writeEndDocument();
    file.close();

    QApplication::restoreOverrideCursor();

    m_pSettings->setValue("project_file_path", m_sFilePath);
}
