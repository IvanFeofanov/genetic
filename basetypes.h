#ifndef BASETYPES_H
#define BASETYPES_H

#include <vector>
#include <QLineF>
#include <QSizeF>
#include <set>


/// @brief Структура описания линии связи
struct SLinkLine
{
    SLinkLine() :
        m_nLayerNumber(0),
        m_fWidth(1)
    {}

    int     m_nLayerNumber;     ///< Номер слоя
    float   m_fWidth;           ///< Ширина проводника в px
    QLineF  m_line;             ///< Линия в координатах сцены
};

/// @brief Структура описания схемы
struct SSchem
{
    typedef std::vector<SLinkLine>	TLink;          ///< Связь
    typedef std::pair<QPointF, int>	TElement;		///< Схема: позиция - размер

    QSizeF                  m_sheetSize;    ///< Размеры монтажного пространства
    std::vector<TElement>   m_vecElements;  ///< Элементы
    std::vector<TLink>      m_vecLinks;     ///< связи

    /// Определение числа слоев в схеме
    static int GetSchemLayers(const SSchem& schem_)
    {
        std::set<int> setLayers;
        for(const SSchem::TLink& link : schem_.m_vecLinks)
            for(const SLinkLine& line : link)
                setLayers.insert(line.m_nLayerNumber);

        return std::max((int)setLayers.size(), 1);
    }
};

#endif // BASETYPES_H
