#include "dlgbuild.h"
#include "ui_dlgbuild.h"

#include <QDateTime>

#include "schemtographbuilder.h"

CDlgBuild::CDlgBuild(QWidget *pParent_)
    : QDialog(pParent_)
    , m_pUi(new Ui::dlgBuild)
{
    m_pUi->setupUi(this);

    QButtonGroup* bg = new QButtonGroup(this);
    bg->addButton(m_pUi->rbMethodA1);
    bg->addButton(m_pUi->rbMethodA2);

    connect(m_pUi->pbBuild, SIGNAL(clicked(bool)), this, SLOT(build()));
}

CDlgBuild::~CDlgBuild()
{
    delete m_pUi;
}

void CDlgBuild::SetSchem(const SSchem &schem_)
{
    m_schem = schem_;
    CSchemToGraphBuilder::MakeGraph(m_schem.m_vecLinks, m_graph);
    m_pUi->wgtGraphViewer->SetGraph(m_graph);
}

void CDlgBuild::build()
{
    QDateTime start = QDateTime::currentDateTime();

    // Окраска графа выбранным алгоритмом
    if(m_pUi->rbMethodA1)
        CColorizer::MethodA1(m_graph);
    else if(m_pUi->rbMethodA2)
        CColorizer::MethodA2(m_graph);

    CColorizer::DistByLayers(m_graph, m_schem);

    QDateTime finish = QDateTime::currentDateTime();
    int secs = finish.secsTo(start);
    start.addSecs(secs);
    int msecs = finish.time().msecsTo(start.time());
    int duration = secs * 1000 + msecs;

    // Время выполнения
    m_pUi->lblTime->setText( duration > 1 ? QString::number(duration) : "<1");

    // Хромтическое число
    m_pUi->lblChromNumber->setText(QString::number(m_graph.GetChromatic()));

    // Представление графа
    m_pUi->wgtGraphViewer->SetGraph(m_graph);

    // сообщаем о завершении
    emit buildFinished();
}
