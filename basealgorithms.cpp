#include "basealgorithms.h"
#include <algorithm>
#include <qrgb.h>
#include <assert.h>

float SquareDistance(const QPointF& p1_, const QPointF& p2_)
{
    return std::pow(p1_.x() - p2_.x(), 2) + pow(p1_.y() - p2_.y(), 2);
}

float Distance(const QPointF &p1_, const QPointF &p2_)
{
    return std::sqrt(SquareDistance(p1_, p2_));
}

void GeneratePalette(std::vector<QColor> &palette_, int nColor_, int nAlfa_)
{
    if(nColor_ <= 1)
    {
        palette_.push_back(Qt::green);
        palette_[0].setAlpha(nAlfa_);
        return;
    }

    int step = 359 / nColor_;
    for(int i = 0; i < nColor_; i++)
        palette_.push_back(QColor::fromHsl(step * i, 255, 127, nAlfa_));
}

QColor GenerateAndGetColorByIndex(int nColors_, int nIndex_, int nAlfa_)
{
    assert(nIndex_ < nColors_);
    std::vector<QColor> palette;
    GeneratePalette(palette, nColors_, nAlfa_);
    return palette[nIndex_];
}
