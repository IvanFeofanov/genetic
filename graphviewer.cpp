#include "graphviewer.h"
#include "ui_graphviewer.h"

#include "basealgorithms.h"

#include <math.h>

#include <QGraphicsLineItem>
#include <QPen>
#include <QBrush>

#include <QDebug>
#include <assert.h>
#include <cmath>

static int c_nSceneSize = 100;
static int c_nVertexMaxRadius = 10; // Максимальный радиус вершины

// Параметоры отображения графа
static int c_nGraphRadius = c_nSceneSize / 2 - 10;
static QPointF c_centerPoint(c_nSceneSize / 2, c_nSceneSize / 2);


const double M_PI = 3.14;

static double gradToRad(double dGrad_)
{
    return dGrad_ * M_PI / 180.f;
}

//========================================================================================
CGraphViewer::CGraphViewer(QWidget *pParent_) :
    QWidget(pParent_),
    m_pUi(new Ui::CGraphViewer)
{
    m_pUi->setupUi(this);

    QGraphicsScene* pScene = new QGraphicsScene(0, 0, c_nSceneSize, c_nSceneSize);
    m_pUi->gvGraph->setScene(pScene);
    m_pUi->gvGraph->setRenderHint(QPainter::Antialiasing);
}

CGraphViewer::~CGraphViewer()
{
    delete m_pUi;
}

void CGraphViewer::Clear()
{
    QGraphicsScene* pScene =  m_pUi->gvGraph->scene();
    assert(pScene);
    pScene->clear();
}

void CGraphViewer::SetGraph(const CNonDirGraph &graph_)
{
    Clear();

    // Стиль линий
    QPen pen(QBrush(Qt::gray), 1);
    pen.setCosmetic(true);

    // Палитра
    std::vector<QColor> vecPalette;
    int chromatic = graph_.GetChromatic();
    GeneratePalette(vecPalette, chromatic);

    // Сцена
    QGraphicsScene* pScene = m_pUi->gvGraph->scene();
    assert(pScene);

    // Количество вершин графа
    int nVertex = graph_.GetVertexCount();

    // Вычисление вершин графа
    std::vector<QPointF> vecPoints;
    calcVertexPoints(nVertex, vecPoints);
    float fVertexRadius = std::min(calcVertexRadius(vecPoints), (float)c_nVertexMaxRadius);

    // Создание элементов вершин
    for(int i = 0; i < vecPoints.size(); i++)
    {
        const QPointF& p = vecPoints[i];

        // цвет
        int nColor = graph_.GetVertex(i).GetColor();
        assert(nColor < chromatic);
        QBrush brush(vecPalette[std::max(nColor, 0)]);

        QGraphicsEllipseItem* pEllipseItem = new QGraphicsEllipseItem(p.x(), p.y(), fVertexRadius , fVertexRadius);
        pEllipseItem->setBrush(brush);
        pEllipseItem->setPen(pen);
        pEllipseItem->setZValue(2);
        pScene->addItem(pEllipseItem);
    }

    // Связывание смежных вершин
    for(int i = 0; i < nVertex; i++)
    {
        const CVertex& v = graph_.GetVertex(i);
        const CVertex::TSetAdj& adj = v.GetAdjVertex();
        for(CVertex::TId id : adj)
        {
            if(id > i) // если вершина еще не связана
            {
                QPointF p1 = vecPoints[i] + QPointF(fVertexRadius / 2, fVertexRadius / 2);
                QPointF p2 = vecPoints[id] + QPointF(fVertexRadius / 2, fVertexRadius / 2);

                QGraphicsLineItem* pLineItem = new QGraphicsLineItem(QLineF(p1, p2));
                pLineItem->setPen(pen);
                pLineItem->setZValue(1);
                pScene->addItem(pLineItem);
            }
        }
    }

    // показать весь граф
    m_pUi->gvGraph->fitInView(pScene->sceneRect(), Qt::KeepAspectRatio);
}

void CGraphViewer::calcVertexPoints(int nVertex_, std::vector<QPointF> &points_)
{
    QGraphicsScene* pScene = m_pUi->gvGraph->scene();
    assert(pScene);

    double angleStep = 360.f / nVertex_;
    for(int i = 0; i < nVertex_; i++)
    {
        double angle = angleStep * i;
        float y = std::sin(gradToRad(angle)) * c_nGraphRadius;
        float x = std::cos(gradToRad(angle)) * c_nGraphRadius;
        points_.push_back(QPointF(x, y) + pScene->sceneRect().center());
    }
}

float CGraphViewer::calcVertexRadius(const std::vector<QPointF> &points_)
{
    if(points_.size() < 2)
        return c_nVertexMaxRadius;

    float fDist = Distance(points_[0], points_[1]);
    return fDist / 2;
}
