#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSettings>

#include "basetypes.h"
#include "graph.h"
#include "dlgbuild.h"


namespace Ui {
class CMainWindow;
}

/// @brief Класс главного окна проекта
class CMainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit CMainWindow(QWidget *pParent_ = 0);
    ~CMainWindow();

private slots:
    /// Создать новый проект
    void actNew_triggered();

    /// Открыть проект
    void actOpen_triggered();

    /// Сохранить проект
    /// TODO перенести сохранение платы в редактор
    void actSave_triggered();

    /// Сохранить проект как
    void actSaveAs_triggered();

    /// Запустить проект
    void actBuild_triggered(bool bEn_);

    /// Раскрашивание завершено
    void colorizeFinished();

private:
    /// Событие закрытия окна
    void closeEvent(QCloseEvent *pEvent_);

private:
    /// Сохранение проекта
    void save(const QString& sFilePath_);

private:
    Ui::CMainWindow*    m_pUi;          ///< UI
    QString             m_sFilePath;    ///< Путь сохранения файла
    QSettings*          m_pSettings;    ///< Сохранение настроек
    CDlgBuild*          m_pDlgBuild;    ///< Диалог выполнения расчетов
};

#endif // MAINWINDOW_H
