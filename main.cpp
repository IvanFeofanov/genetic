#include "mainwindow.h"
#include <QApplication>
#include <QSettings>
#include <QLibraryInfo>
#include <QTranslator>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    // Параметры приложения
    QApplication::setOrganizationName("TSTU");
    QApplication::setApplicationVersion("0.0.0");
    QApplication::setApplicationName("Distribution of communications");

    // Языки
    QTranslator qtTranslator;
    qtTranslator.load("qt_" + QLocale::system().name(),
                      QLibraryInfo::location(QLibraryInfo::TranslationsPath));
    a.installTranslator(&qtTranslator);

    // Главное окно
    CMainWindow w;
    w.showMaximized();

    return a.exec();
}
