#ifndef ELEMETMANAGER_H
#define ELEMETMANAGER_H

#include <QGraphicsItem>
#include <map>
#include <string>
#include <vector>

/// @brief Класс менеджера электронных элементов
class CElemetManager
{
private:
    CElemetManager() {}
    CElemetManager(const CElemetManager&) {}
    CElemetManager& operator=(CElemetManager&);

public:
    typedef QGraphicsItem* (*TPItemCreator)();                  ///< Тип указателя на функцию креатор элемента
    typedef std::map<std::string, TPItemCreator> TMapCreators;  ///< Тип мапа креаторов
    typedef std::vector<std::string> TVecNames;                 ///< Тип массива имен элементов

public:
    /// Получить объект синглтона
    static CElemetManager& GetInstance();

    /// Зарегистрировать создатель элемента
    bool RegisterElement(const std::string& strName_, TPItemCreator pCreator_);

    /// Получить креатор по имени
    TPItemCreator GetElementCreator(const std::string& strName_) const;

    /// Получить список имен всех элементов
    TVecNames GetAllElementNames() const;

private:
    TMapCreators m_mapCreators;
};

#endif // ELEMETMANAGER_H