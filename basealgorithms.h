#ifndef BASEALGORITHMS_H
#define BASEALGORITHMS_H

#include <QPointF>
#include <QColor>
#include <vector>

/// Квадрат расстояния между точками
/// @param p1_ точка 1
/// @param p1_ точка 2
/// @return квадрат расстояния между точками
float SquareDistance(const QPointF& p1_, const QPointF& p2_);

/// Расстояние между точками
/// @param p1_ точка 1
/// @param p1_ точка 2
/// @return расстояния между точками
float Distance(const QPointF& p1_, const QPointF& p2_);

/// Генерировать палитру цветов
/// @param palette - результирующая палитра
/// @param nColor_ - число цветов
/// @param nAlfa_ - прозрачность от 0 до 255
void GeneratePalette(std::vector<QColor>& palette_, int nColor_, int nAlfa_ = 255);

/// Создать палитру и получить цвет по индексу
/// @param nColor_ - число цветов
/// @param nIndex - индекс возвращаемого цвет
/// @param nAlfa_ - прозрачность от 0 до 255
/// @return цвет
QColor GenerateAndGetColorByIndex(int nColors_, int nIndex_, int nAlfa_ = 255);

#endif // BASEALGORITHMS_H