#ifndef GRAPHVIEWER_H
#define GRAPHVIEWER_H

#include <QWidget>
#include <vector>
#include <QPointF>
#include <QGraphicsEllipseItem>

#include "graph.h"

namespace Ui {
class CGraphViewer;
}

/// @brief класс виджета представления графа
class CGraphViewer : public QWidget
{
    Q_OBJECT

public:
    explicit CGraphViewer(QWidget *pParent_ = 0);
    ~CGraphViewer();

    /// Установить граф
    /// @param graph_ граф
    void SetGraph(const CNonDirGraph& graph_);

public slots:
    /// Очистить граф
    void Clear();

private:
    /// Вычислить координаты вершин графа
    /// @param nVertex_ количество вершин
    /// @param points_ результирующий вектор координат вершин
    void calcVertexPoints(int nVertex_, std::vector<QPointF> &points_);

    /// Вычислить радиус вершины графа
    /// @param вектор координат вершин
    /// @return радиус вершин
    float calcVertexRadius(const std::vector<QPointF>& points_);

private:
    Ui::CGraphViewer *m_pUi;
};

#endif // GRAPHVIEWER_H
